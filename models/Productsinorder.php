<?php

namespace app\models;
use yii\db\ActiveRecord;
use app\models\Menuitem;

use Yii;

/**
 * This is the model class for table "productsinorder".
 *
 * @property string $productnumber
 * @property string $quantity
 * @property integer $orderid
 */
class Productsinorder extends \yii\db\ActiveRecord
{
	
    public static function tableName()
    {
        return 'productsinorder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['productnumber', 'orderid'], 'required'],
            [['orderid'], 'integer'],
            [['productnumber', 'quantity'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'productnumber' => 'Productnumber',
            'quantity' => 'Quantity',
            'orderid' => 'Orderid',
        ];
    }
	
	public function getItemName() {
		return $this->hasOne(Menuitem::className(), ['id' => 'productnumber']);
	}
}
