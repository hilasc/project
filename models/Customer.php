<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $phone
 * @property string $address
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'phone'], 'required'],
            [['firstname', 'lastname', 'phone', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'First name',
            'lastname' => 'Last name',
            'phone' => 'Phone',
            'address' => 'Address',
        ];
    }
	
	public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }
	
	//helps to get all users (map from id to full name)
	public static function getCustomers()
	{
		$users = ArrayHelper::
					map(self::find()->all(), 'id', 'fullname');
		return $users;						
	}
	
	//return array of all users first names (with "all users" options) - for the search owner dropdown
	public static function getUsersWithAllUsers()
	{
		$users = self::getCustomers();
		$users[-1] = 'All Users';
		$users = array_reverse ( $users, true );
		return $users;	
	}
	
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}
}
