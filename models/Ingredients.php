<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ingredients".
 *
 * @property integer $id
 * @property string $ingredientname
 */
class Ingredients extends \yii\db\ActiveRecord
{
	public $role;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ingredients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ingredientname'], 'string', 'max' => 255],
			['role', 'safe'],
        ];
    }
	
	//helps to get all customers (map from id to full name)
	public static function getIngridient()
	{
		$ingridient = ArrayHelper::
					map(self::find()->all(), 'id', 'ingredientname');
		return $ingridient;						
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ingredientname' => 'Ingredient name',
        ];
    }
	
	/*public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);

        if (!\Yii::$app->user->can('updateIngredients')){
			return $return;
		}
		$auth = Yii::$app->authManager;
		$roleName = $this->role; 
		$role = $auth->getRole($roleName);
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
			$auth->assign($role, $this->id);
		} else {
			$db = \Yii::$app->db;
			$db->createCommand()->delete('auth_assignment',
				['user_id' => $this->id])->execute();
			$auth->assign($role, $this->id);
		}

        return $return;
    }*/
	
	public static function getRoles()
	{
		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name; 
		}
		return $roles; 	
	}
}
