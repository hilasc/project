<?php

namespace app\models;
use yii\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "ingredinorder".
 *
 * @property integer $inventoryorderid
 * @property string $ingrediantid
 * @property string $quantity
 */
class Ingredinorder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ingredinorder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ingrediantid'], 'required'],
            [['ingrediantid', 'quantity'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inventoryorderid' => 'Inventoryorderid',
            'ingrediantid' => 'Ingrediantid',
            'quantity' => 'Quantity',
        ];
    }
}
