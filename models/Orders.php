<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use app\models\Productsinorder;
use app\models\Menuitem;
use app\models\OrderToExport;


/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property string $orderdate
 * @property string $orderprice
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $customerid
 */
class Orders extends \yii\db\ActiveRecord
{	
	public $item1;
	public $quantity1;
	public $item2;
	public $quantity2;
	public $item3;
	public $quantity3;


    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'customerid', 'status',], 'integer'],
            [['orderprice'], 'string', 'max' => 255],
			[['orderdate',], 'date', 'format' => 'php:d/m/Y'],
			[['item1', 'quantity1','item2', 'quantity2', 'item3', 'quantity3'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orderdate' => 'Order date (dd/mm/yyyy)',
            'orderprice' => 'Order price',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'customerid' => 'Customer name',
			'item1' => 'Item 1',
			'quantity1' => 'Qty 1',
			'item2' => 'Item 2',
			'quantity2' => 'Qty 2',
			'item3' => 'Item 3',
			'quantity3' => 'Qty 3',
			'status' => 'Status',
        ];
    }
	
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
	
	// customer name for this order instead of the id
	public function getCustomerName()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customerid']);
    }
	
	public static function getStatusById($id)
	{
		return Status::findOne(['id' => $id])->name;
	}
	
	public static function getCustomerNameById($id) {
		return Customer::findOne(['id' => $id])->getFullname();
	}
	
	public static function getAllItems()
	{
		$items = ArrayHelper::
					map(Menuitem::find()->all(), 'id', 'itemname');
		return $items;		
		
	}
	
	public static function getItemsInOrder($id){
		$items=Productsinorder::findAll(['orderid' => $id,
		]);
		
		//$allItemsArray = ArrayHelper::
			//		map($items, 'productnumber', 'quantity');
		return $items;
	}
	
	public static function getOrdersToExport() {
		$allOrders = Orders::find()->all();
		$result = array();
		foreach ($allOrders as $order) {
			$orderExport = new OrderToExport();
			$orderExport->orderId = $order->id;
			$orderExport->customerId = Orders::getCustomerNameById($order->customerid);
			$orderExport->status = Orders::getStatusById($order->status);
			$orderExport->orderDate = $order->orderdate;
			$orderExport->orderPrice = $order->orderprice;
			
			$items = Orders::getItemsInOrder($order->id);
			$orderExport->item1 = isset($items[0]) ? $items[0]->itemName->itemname : '----';
			$orderExport->qty1 = isset($items[0]) ? $items[0]->quantity : '----';
			$orderExport->item2 = isset($items[1]) ? $items[1]->itemName->itemname : '----';
			$orderExport->qty2 = isset($items[1]) ? $items[1]->quantity : '----';
			$orderExport->item3 = isset($items[2]) ? $items[2]->itemName->itemname : '----';
			$orderExport->qty3 = isset($items[2]) ? $items[2]->quantity : '----';

			array_push($result,$orderExport);
		}
		return $result;
	}
	
	public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }
	
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}
	
		//getCreatedBy function is to show who created the Orders in the Orders form, instead of the id
	public function getCreatedBy()
    {
        return $this->hasOne(Employee::className(), ['id' => 'created_by']);
    }	
	
	//getUpdateddBy function is to show who created the Orders in the Orders form, instead of the id
	public function getUpdateddBy()
    {
        return $this->hasOne(Employee::className(), ['id' => 'updated_by']);
    }
	
	public function getItemPriceByItemId($itemId, $quantity)
	{
		$item = Menuitem::findOne($itemId);
		return (isset($item) ? $item->itemprice * $quantity : 0);
	}
	
	public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);
		
		Productsinorder::deleteAll(['orderid' => $this->id]);
		
		$itemId1 = $this->item1;
		$quantity1 = $this->quantity1;
		$itemId2 = $this->item2;
		$quantity2 = $this->quantity2;
		$itemId3 = $this->item3;
		$quantity3 = $this->quantity3;
		$orderprice=0;
		
		if ($itemId1 != NULL) {
			$pio1 = new Productsinorder();
			$pio1->orderid = $this->id;
			$pio1->productnumber = $itemId1;
			$pio1->quantity = $quantity1;
			$orderprice += Orders::getItemPriceByItemId($itemId1, $quantity1);
			$pio1->save(false);
		}	
		
		if ($itemId2 != NULL) {
			$pio2 = new Productsinorder();
			$pio2->orderid = $this->id;
			$pio2->productnumber = $itemId2;
			$pio2->quantity = $quantity2;
			$orderprice += Orders::getItemPriceByItemId($itemId2, $quantity2);
			$pio2->save(false);
		}
		
		if ($itemId3 != NULL) {
			$pio3 = new Productsinorder();
			$pio3->orderid = $this->id;
			$pio3->productnumber = $itemId3;
			$pio3->quantity = $quantity3;
			$orderprice += Orders::getItemPriceByItemId($itemId3, $quantity3);
			$pio3->save(false);
		}
		
			Yii::$app->db->createCommand()->update(self::tableName(), ['orderprice'=>$orderprice], ['id' => $this->id])->execute();
        
		return $return;
    }
}
