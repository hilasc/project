<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use app\models\Productsinorder;
use app\models\Menuitem;


class OrderToExport
{
	public $orderId;
	public $customerId;
	public $status;
	public $orderDate;
	public $orderPrice;
	public $item1;
	public $qty1;
	public $item2;
	public $qty2;
	public $item3;
	public $qty3;
	
	
	public function getAttributeLabels()
    {
        return [
			'orderId' => 'Order ID',
            'orderDate' => 'Order date',
            'orderPrice' => 'Order price',
            'customerId' => 'Customer ID',
			'item1' => 'Item 1',
			'qty1' => 'Qty 1',
			'item2' => 'Item 2',
			'qty2' => 'Qty 2',
			'item3' => 'Item 3',
			'qty3' => 'Qty 3',
			'status' => 'Status',
        ];
    }
	
	
	
}



?>
