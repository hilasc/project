<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "inventoryorders".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $ingredient1
 * @property integer $ingredient2
 * @property integer $ingredient3
 * @property integer $ingredient4
 * @property integer $quantity1
 * @property integer $quantity2
 * @property integer $quantity3
 * @property integer $quantity4
 */
class Inventoryorders extends \yii\db\ActiveRecord
{
	public $role;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inventoryorders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['ingredient1','ingredient2','ingredient3','ingredient4', 'quantity1', 'quantity2', 'quantity3', 'quantity4'], 'string', 'max' => 255],
			['role', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'ingredient1' => 'Ingredient 1',
            'ingredient2' => 'Ingredient 2',
            'ingredient3' => 'Ingredient 3',
            'ingredient4' => 'Ingredient 4',
            'quantity1' => 'Quantity for Ing 1',
            'quantity2' => 'Quantity for Ing 2',
            'quantity3' => 'Quantity for Ing 3',
            'quantity4' => 'Quantity for Ing 4',
        ];
    }
	
		
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	
	/*public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);

        if (!\Yii::$app->user->can('updateInventoryorders')){
			return $return;
		}
		$auth = Yii::$app->authManager;
		$roleName = $this->role; 
		$role = $auth->getRole($roleName);
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
			$auth->assign($role, $this->id);
		} else {
			$db = \Yii::$app->db;
			$db->createCommand()->delete('auth_assignment',
				['user_id' => $this->id])->execute();
			$auth->assign($role, $this->id);
		}

        return $return;
    }*/
	
	public static function getInventoryOrdersToExport() {
		$allInventory = Inventoryorders::find()->all();
		$result = array();
		foreach ($allInventory as $inventoryorders) {
			$orderExport = new Inventoryorders();
			$orderExport->id = $inventoryorders->id;
			$orderExport->ingredient1 = isset($inventoryorders->ingredient1) ?(Inventoryorders::getIngridientNameById($inventoryorders->ingredient1)) : '----';
			$orderExport->ingredient2 = isset($inventoryorders->ingredient2) ?(Inventoryorders::getIngridientNameById($inventoryorders->ingredient2)) : '----';
			$orderExport->ingredient3 = isset($inventoryorders->ingredient3) ?(Inventoryorders::getIngridientNameById($inventoryorders->ingredient3)) : '----';
			$orderExport->ingredient4 = isset($inventoryorders->ingredient4) ?(Inventoryorders::getIngridientNameById($inventoryorders->ingredient4)) : '----';
			$orderExport->quantity1 = isset($inventoryorders->quantity1) ? $inventoryorders->quantity1 : '----';
			$orderExport->quantity2 = isset($inventoryorders->quantity2) ? $inventoryorders->quantity2 : '----';
			$orderExport->quantity3 = isset($inventoryorders->quantity3) ? $inventoryorders->quantity3 : '----';
			$orderExport->quantity4 = isset($inventoryorders->quantity4) ? $inventoryorders->quantity4 : '----';
			array_push($result,$orderExport);
		}
		return $result;
	}
	
	public static function getRoles()
	{
		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name; 
		}
		return $roles; 	
	}
	
	public function getIngridient1Name()
    {
        return $this->hasOne(Ingredients::className(), ['id' => 'ingredient1']);
    }
	
	public static function getIngridientNameById($id) {
		return Ingredients::findOne(['id' => $id])->ingredientname;
	}
	
	public function getIngridient2Name()
    {
        return $this->hasOne(Ingredients::className(), ['id' => 'ingredient2']);
    }
	
	public function getIngridient3Name()
    {
        return $this->hasOne(Ingredients::className(), ['id' => 'ingredient3']);
    }
	
	public function getIngridient4Name()
    {
        return $this->hasOne(Ingredients::className(), ['id' => 'ingredient4']);
    }
	
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}
	
	public function getIngredientItem1()
    {
        return $this->hasOne(Ingredients::className(), ['id' => 'ingredient1']);
    }
	
	public function getIngredientItem2()
    {
        return $this->hasOne(Ingredients::className(), ['id' => 'ingredient2']);
    }
	
	public function getIngredientItem3()
    {
        return $this->hasOne(Ingredients::className(), ['id' => 'ingredient3']);
    }
	
	public function getIngredientItem4()
    {
        return $this->hasOne(Ingredients::className(), ['id' => 'ingredient4']);
    }
	

	//getCreatedBy function is to show who created the Inventoryorders in the Inventoryorders form, instead of the id
	public function getCreatedBy()
    {
        return $this->hasOne(Employee::className(), ['id' => 'created_by']);
    }	
	
	//getUpdateddBy function is to show who created the Inventoryorders in the Inventoryorders form, instead of the id
	public function getUpdateddBy()
    {
        return $this->hasOne(Employee::className(), ['id' => 'updated_by']);
    }

}
