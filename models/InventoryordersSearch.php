<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Inventoryorders;

/**
 * InventoryordersSearch represents the model behind the search form about `app\models\Inventoryorders`.
 */
class InventoryordersSearch extends Inventoryorders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'ingredient1', 'ingredient2', 'ingredient3', 'ingredient4', 'quantity1', 'quantity2', 'quantity3', 'quantity4'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Inventoryorders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'ingredient1' => $this->ingredient1,
            'ingredient2' => $this->ingredient2,
            'ingredient3' => $this->ingredient3,
            'ingredient4' => $this->ingredient4,
            'quantity1' => $this->quantity1,
            'quantity2' => $this->quantity2,
            'quantity3' => $this->quantity3,
            'quantity4' => $this->quantity4,
        ]);

        return $dataProvider;
    }
}
