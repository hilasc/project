<?php

namespace app\controllers;

use Yii;
use app\models\Inventoryorders;
use app\models\InventoryordersSearch;
use app\models\Menuitem;
use app\models\Employee;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;

/**
 * InventoryordersController implements the CRUD actions for Inventoryorders model.
 */
class InventoryordersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Inventoryorders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InventoryordersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'ingredient1' => $searchModel->ingredient1,
        ]);
    }
	
	public function actionExport()
    {
		$inventoryExport = Inventoryorders::getInventoryOrdersToExport();
		\moonland\phpexcel\Excel::widget([
			'models' => $inventoryExport,
			'mode' => 'export', //default value as 'export'
			'columns' => ['id','ingredient1', 'quantity1', 'ingredient2', 'quantity2', 'ingredient3', 'quantity3', 'ingredient4', 'quantity4'], //without header working, because the header will be get label from attribute label. 
			'headers' => ['id' => 'Inventory Order ID', 'ingredient1' => 'Ingredient 1', 'quantity1' => 'Quantity 1', 'ingredient2' => 'Ingredient 2', 'quantity2' => 'Quantity 2', 'ingredient3' => 'Ingredient 3', 'quantity3' => 'Quantity 3', 'ingredient4' => 'Ingredient 4','quantity4' => 'Quantity 4'], 
			'fileName' => 'Inventory Orders',
	
	]);
	}

    /**
     * Displays a single Inventoryorders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Inventoryorders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		if (!\Yii::$app->user->can('createInventoryorders'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to create new users');
        $model = new Inventoryorders();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$roles = Employee::getRoles(); 
            return $this->render('create', [
                'model' => $model,
				'roles' => $roles,
            ]);
        }
    }

    /**
     * Updates an existing Inventoryorders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		if (!\Yii::$app->user->can('updateInventoryorders'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to update users');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$roles = Employee::getRoles(); 
			return $this->render('update', [
				'model' => $model,
				'roles' => $roles,
            ]);
        }
    }

    /**
     * Deletes an existing Inventoryorders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if (!\Yii::$app->user->can('deleteInventoryorders'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to delete users');		
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Inventoryorders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Inventoryorders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Inventoryorders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
