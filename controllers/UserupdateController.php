<?php

//using "controllers folder instead of "commands"
namespace app\controllers;

use Yii;
//using "web" instead of "console"
use yii\web\Controller;


class UserupdateController extends Controller
{
	public function actionAddrule()
	{
		$auth = Yii::$app->authManager;				
		$updateOwnUser = $auth->getPermission('updateOwnUser');
		$auth->remove($updateOwnUser);
		
		$rule = new \app\rbac\OwnUserRule;
		$auth->add($rule);
				
		$updateOwnUser->ruleName = $rule->name;		
		$auth->add($updateOwnUser);	
	}
}