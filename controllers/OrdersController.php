<?php

namespace app\controllers;

use Yii;
use app\models\Orders;
use app\models\OrdersSearch;
use app\models\Status;
use app\models\Customer;
use app\models\Productsinorder;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'statuses' => Status::getStatusesWithAllStatuses(),
			'status' => $searchModel->status,
			'customerids' => Customer::getUsersWithAllUsers(),
			'customerid' => $searchModel->customerid,
			'item1' => $searchModel->item1,
			'quantity1' => $searchModel->quantity1,
			'item2' => $searchModel->item2,
			'quantity2' => $searchModel->quantity2,
			'item3' => $searchModel->item3,
			'quantity3' => $searchModel->quantity3,
			'customerids' => Customer::getUsersWithAllUsers(),
        ]);
    }
	
	public function actionExport()
    {
		$orderExport = Orders::getOrdersToExport();
		\moonland\phpexcel\Excel::widget([
			'models' => $orderExport,
			'mode' => 'export', //default value as 'export'
			'columns' => ['orderId','customerId','status','orderDate','orderPrice','item1', 'qty1','item2', 'qty2','item3', 'qty3'], //without header working, because the header will be get label from attribute label. 
			'headers' => ['orderId' => 'Order ID', 'orderDate' => 'Order Date','orderPrice' => 'Order Price', 'customerId' => 'Customer ID', 'status' => 'Status', 'item1' => 'Item 1', 'qty1' => 'Qty 1', 'item2' => 'Item 2', 'qty2' => 'Qty 2', 'item3' => 'Item 3', 'qty3' => 'Qty 3'], 
			'fileName' => 'Orders',
	
	]);
		
		
		/*return $this->render('export', [
            'allModels' => $orderExport,
			]);*/
    }
	
	

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$orderModel = $this->findModel($id);
        return $this->render('view', [
            'model' => $orderModel,
			'itemsInOrder' => Orders::getItemsInOrder($id),
        ]);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //access control
		$model = new Orders();
				
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$items = Orders::getAllItems(); 
            return $this->render('create', [
                'model' => $model,
				'items' => $items,
            ]);
        }
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$itemsInOrder = Orders::getItemsInOrder($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
		    return $this->redirect(['view', 'id' => $model->id]);
			
        } else {
			$items = Orders::getAllItems(); 
            return $this->render('update', [
                'model' => $model,
				'items' => $items,
				'itemsInOrder' => $itemsInOrder,
            ]);
        }
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
		
		Productsinorder::deleteAll(['orderid' => $model->id]);
		
		$model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
