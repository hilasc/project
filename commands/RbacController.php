<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class RbacController extends Controller
{
	public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$teammember = $auth->createRole('teammember');
		$auth->add($teammember);

		$admin = $auth->createRole('admin');
		$auth->add($admin);
	}

	public function actionTmpermissions()
	{
		$auth = Yii::$app->authManager;

		$updateOwnUser = $auth->createPermission('updateOwnUser');
		$updateOwnUser->description = 'Every user can update his/her
									own profile ';
		$auth->add($updateOwnUser);
		 
		$updateOwnPassword  = $auth->createPermission('updateOwnPassword');
		$updateOwnPassword->description = 'VEvery user can update his/her
									own password';
		$auth->add($updateOwnPassword);		
	}
	

	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createUser = $auth->createPermission('createUser');
		$createUser->description = 'Admin can create new users';
		$auth->add($createUser);
		
		$updateUser = $auth->createPermission('updateUser');
		$updateUser->description = 'Admin can update all users';
		$auth->add($updateUser);

		$deleteUser = $auth->createPermission('deleteUser');
		$deleteUser->description = 'Admin can delete users';
		$auth->add($deleteUser);
	
		$updatePassword = $auth->createPermission('updatePassword');
		$updatePassword->description = 'Admin can update password for 
									all users';
		$auth->add($updatePassword);
		
		$deleteCustomer = $auth->createPermission('deleteCustomer');
		$deleteCustomer->description = 'Admin can delete customers';
		$auth->add($deleteCustomer);

		$deleteOrder = $auth->createPermission('deleteOrder');
		$deleteOrder->description = 'Admin can delete orders';
		$auth->add($deleteOrder);
		
		$createMenuitem = $auth->createPermission('createMenuitem');
		$createMenuitem->description = 'Admin can create new Menu item';
		$auth->add($createMenuitem);
		
		$deleteMenuitem = $auth->createPermission('deleteMenuitem');
		$deleteMenuitem->description = 'Admin can delete Menu item';
		$auth->add($deleteMenuitem);
		
		$updateMenuitem = $auth->createPermission('updateMenuitem');
		$updateMenuitem->description = 'Admin can update all Menu items';
		$auth->add($updateMenuitem);
		
		$createInventoryorders = $auth->createPermission('createInventoryorders');
		$createInventoryorders->description = 'Admin can create new Inventory order';
		$auth->add($createInventoryorders);
		
		$deleteInventoryorders = $auth->createPermission('deleteInventoryorders');
		$deleteInventoryorders->description = 'Admin can delete Inventory orders';
		$auth->add($deleteInventoryorders);
		
		$updateInventoryorders = $auth->createPermission('updateInventoryorders');
		$updateInventoryorders->description = 'Admin can update all Inventory orders';
		$auth->add($updateInventoryorders);
		
		$createIngredients = $auth->createPermission('createIngredients');
		$createIngredients->description = 'Admin can create new Ingredient';
		$auth->add($createIngredients);
		
		$deleteIngredients = $auth->createPermission('deleteIngredients');
		$deleteIngredients->description = 'Admin can delete Ingredients';
		$auth->add($deleteIngredients);
		
		$updateIngredients = $auth->createPermission('updateIngredients');
		$updateIngredients->description = 'Admin can update all Ingredients';
		$auth->add($updateIngredients);
	}

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$teammember = $auth->getRole('teammember');

		$updateOwnUser = $auth->getPermission('updateOwnUser');
		$auth->addChild($teammember, $updateOwnUser);		
		
		$updateOwnPassword = $auth->getPermission('updateOwnPassword');
		$auth->addChild($teammember, $updateOwnPassword);		

		$admin = $auth->getRole('admin');	
		
		$createUser = $auth->getPermission('createUser');
		$auth->addChild($admin, $createUser);

		$updateUser = $auth->getPermission('updateUser');
		$auth->addChild($admin, $updateUser);
		
		$deleteUser = $auth->getPermission('deleteUser');
		$auth->addChild($admin, $deleteUser);		

		$updatePassword = $auth->getPermission('updatePassword');
		$auth->addChild($admin, $updatePassword);
		
		$deleteCustomer = $auth->getPermission('deleteCustomer');
		$auth->addChild($admin, $deleteCustomer);
		
		$deleteOrder = $auth->getPermission('deleteOrder');
		$auth->addChild($admin, $deleteOrder);
		
		$createMenuitem = $auth->getPermission('createMenuitem');
		$auth->addChild($admin, $createMenuitem);
		
		$deleteMenuitem = $auth->getPermission('deleteMenuitem');
		$auth->addChild($admin, $deleteMenuitem);
		
		$updateMenuitem = $auth->getPermission('updateMenuitem');
		$auth->addChild($admin, $updateMenuitem);
		
		$createInventoryorders = $auth->getPermission('createInventoryorders');
		$auth->addChild($admin, $createInventoryorders);
		
		$deleteInventoryorders = $auth->getPermission('deleteInventoryorders');
		$auth->addChild($admin, $deleteInventoryorders);
		
		$updateInventoryorders = $auth->getPermission('updateInventoryorders');
		$auth->addChild($admin, $updateInventoryorders);
		
		$createIngredients = $auth->getPermission('createIngredients');
		$auth->addChild($admin, $createIngredients);
		
		$deleteIngredients = $auth->getPermission('deleteIngredients');
		$auth->addChild($admin, $deleteIngredients);
		
		$updateIngredients = $auth->getPermission('updateIngredients');
		$auth->addChild($admin, $updateIngredients);
	}
}