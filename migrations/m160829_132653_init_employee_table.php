<?php

use yii\db\Migration;

class m160829_132653_init_employee_table extends Migration
{
    public function up()
    {
		$this->createTable (
		'employee',
			[
				'id' => 'pk',
				'username' => 'string',
				'password' => 'string',
				'auth_key' => 'string'
			]
		);
    }

    public function down()
    {
			$this->dropTable('employee');
		}
}
