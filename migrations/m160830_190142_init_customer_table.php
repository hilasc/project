<?php

use yii\db\Migration;

class m160830_190142_init_customer_table extends Migration
{
    public function up()
    {
		$this->createTable (
		'customer',
			[
				'id' => 'pk',
				'firstname' => 'string',
				'lastname' => 'string',
				'phone' => 'string',
				'address' => 'string'
			]
		);
    }
    public function down()
    {
		$this->dropTable('customer');
	}
}
