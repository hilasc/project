<?php

use yii\db\Migration;

class m160831_161223_alter_orders_table extends Migration
{
    public function up()
    {
		$this->addColumn('orders','created_at','integer');
		$this->addColumn('orders','updated_at','integer');
		$this->addColumn('orders','created_by','integer');
		$this->addColumn('orders','updated_by','integer');
    }

    public function down()
    {
        $this->dropTable('orders');
    }
}
