<?php

use yii\db\Migration;

class m160924_172507_alter_inventoryorders_table extends Migration
{
    public function up()
    {
		$this->addColumn('inventoryorders','quantity1','integer');
		$this->addColumn('inventoryorders','quantity2','integer');
		$this->addColumn('inventoryorders','quantity3','integer');
		$this->addColumn('inventoryorders','quantity4','integer');
    }

    public function down()
    {
		$this->dropTable('inventoryorders');

    }

}
