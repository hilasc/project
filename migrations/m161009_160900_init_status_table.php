<?php

use yii\db\Migration;

class m161009_160900_init_status_table extends Migration
{
    public function up()
    {
		$this->createTable(
            'status',
            [
                'id' => 'pk',
                'name' => 'string',				
            ],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
        $this->dropTable('status');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
