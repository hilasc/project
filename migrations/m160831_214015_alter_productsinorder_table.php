<?php

use yii\db\Migration;

class m160831_214015_alter_productsinorder_table extends Migration
{
    public function up()
    {
		$this->addColumn('productsinorder','orderid','integer');
    }

    public function down()
    {
        $this->dropTable('productsinorder');
    }
}
