<?php

use yii\db\Migration;

class m160831_151729_init_inventoryorders_table extends Migration
{
    public function up()
    {
		$this->createTable (
		'inventoryorders',
			[
				'id' => 'pk',
				'ingredientid' => 'string',
				'quantity' => 'string',
				'created_at' => 'integer',
				'updated_at' => 'integer',
				'created_by' => 'integer',
				'updated_by' => 'integer'
			]
		);
    }

    public function down()
             {
			$this->dropTable('inventoryorders');
		}
}
