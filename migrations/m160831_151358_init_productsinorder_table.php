<?php

use yii\db\Migration;

class m160831_151358_init_productsinorder_table extends Migration
{
    public function up()
    {
		$this->createTable (
		'productsinorder',
			[
				'id' => 'pk',
				'productnumber' => 'string',
				'quantity' => 'string'
			]
		);
    }

    public function down()
        {
			$this->dropTable('productsinorder');
		}
}
