<?php

use yii\db\Migration;

class m160829_132902_alter_employee_table extends Migration
{
    public function up()
    {
	    $this->addColumn('employee','ssn','string');
		$this->addColumn('employee','firstname','string');
		$this->addColumn('employee','lastname','string');
		$this->addColumn('employee','email','string');
		$this->addColumn('employee','phone','string');
		$this->addColumn('employee','startDate','string');
		$this->addColumn('employee','endDate','string');
		$this->addColumn('employee','created_at','integer');
		$this->addColumn('employee','updated_at','integer');
		$this->addColumn('employee','created_by','integer');
		$this->addColumn('employee','updated_by','integer');
    }

    public function down()
    {
        $this->dropTable('employee');
    }

  
}
