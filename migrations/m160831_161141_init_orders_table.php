<?php

use yii\db\Migration;

class m160831_161141_init_orders_table extends Migration
{
    public function up()
    {
		$this->createTable (
		'orders',
			[
				'id' => 'pk',
				'orderdate' => 'string',
				'orderprice' => 'string'
			]
		);
    }

    public function down()
    {
			$this->dropTable('orders');
		}
}
