<?php

use yii\db\Migration;

class m160831_212250_alter_orders_table extends Migration
{
    public function up()
    {
		$this->addColumn('orders','customerid','integer');
    }

    public function down()
    {
        $this->dropTable('orders');
    }
}
