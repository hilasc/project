<?php

use yii\db\Migration;

class m160902_194831_init_ingredinorder_table extends Migration
{
    public function up()
    {
		$this->createTable (
		'ingredinorder',
			[
				'inventoryorderid' => 'pk',
				'ingrediantid' => 'string',
				'quantity' => 'string'
			]
		);
    }

    public function down()
        {
			$this->dropTable('ingredinorder');
		}
}
