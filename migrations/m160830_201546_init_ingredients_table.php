<?php

use yii\db\Migration;

class m160830_201546_init_ingredients_table extends Migration
{
    public function up()
    {
		$this->createTable (
		'ingredients',
			[
				'id' => 'pk',
				'ingredientname' => 'string',
				'quantity' => 'string'
			]
		);
    }
	
    public function down()
            {
			$this->dropTable('ingredients');
		}
}
