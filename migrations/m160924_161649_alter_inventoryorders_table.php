<?php

use yii\db\Migration;

class m160924_161649_alter_inventoryorders_table extends Migration
{
    public function up()
    {
		$this->addColumn('inventoryorders','ingridient1','integer');
		$this->addColumn('inventoryorders','ingridient2','integer');
		$this->addColumn('inventoryorders','ingridient3','integer');
		$this->addColumn('inventoryorders','ingridient4','integer');
    }

    public function down()
    {
		$this->dropTable('inventoryorders');
    }

}
