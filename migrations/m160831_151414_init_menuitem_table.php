<?php

use yii\db\Migration;

class m160831_151414_init_menuitem_table extends Migration
{
    public function up()
    {
		$this->createTable (
		'menuitem',
			[
				'id' => 'pk',
				'itemname' => 'string',
				'created_at' => 'integer',
				'updated_at' => 'integer',
				'created_by' => 'integer',
				'updated_by' => 'integer'
			]
		);
    }

    public function down()
           {
			$this->dropTable('menuitem');
		}
}
