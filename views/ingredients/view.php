<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ingredients */

$this->title = 'Ingredient #' .$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ingredients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredients-view">

    <h1><?= Html::encode($this->title) ?></h1>
	</br></br>
	<div class="col-md-offset-3" style="text-align:right;  width: 100%;">
	<?php if (\Yii::$app->user->can('deleteIngredients')) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary','style' => 'float:left; margin-right:10px; margin-left:15px;']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
			'style' => 'float:left;',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
	<?php } ?>
</div>
</br></br></br>
<div class="col-md-5 col-md-offset-3">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ingredientname',
        ],
    ]) ?>
	</div>
</br></br>
</div>
