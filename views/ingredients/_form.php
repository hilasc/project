<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ingredients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ingredients-form">
</br>
</br>
    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<?= $form->field($model, 'ingredientname')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
</br>
</br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
