<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Inventoryorders */

$this->title = 'Inventory order #' .$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inventory orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventoryorders-view">

    <h1><?= Html::encode($this->title) ?></h1>
	<div style="text-align:right;  width: 100%;">
	<?php if (\Yii::$app->user->can('updateInventoryorders')) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary','style' => 'float:left; margin-right:10px;']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
			'style' => 'float:left;',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
	<?php } ?>
</div>
</br></br>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
			
            [ // the ingredient1 name 
				'label' => $model->attributeLabels()['ingredient1'],
				'value' => $model->ingredientItem1 != NULL ? $model->ingredientItem1->ingredientname :  'No ingredient was choosen',	
			],
			[ // quantity1  
				'label' => $model->attributeLabels()['quantity1'],
				'value' => $model->quantity1 != NULL ? $model->quantity1 :  'No ingredient was choosen',	
			],
			[ // the ingredient2 name 
				'label' => $model->attributeLabels()['ingredient2'],
				'value' => $model->ingredientItem2 != NULL ? $model->ingredientItem2->ingredientname :  'No ingredient was choosen',	
			],
			[ // quantity2  
				'label' => $model->attributeLabels()['quantity2'],
				'value' => $model->quantity2 != NULL ? $model->quantity2 :  'No ingredient was choosen',	
			],
			[ // the ingredient3 name 
				'label' => $model->attributeLabels()['ingredient3'],
				'value' => $model->ingredientItem3 != NULL ? $model->ingredientItem3->ingredientname :  'No ingredient was choosen',	
			],
			[ // quantity3  
				'label' => $model->attributeLabels()['quantity3'],
				'value' => $model->quantity3 != NULL ? $model->quantity3 :  'No ingredient was choosen',	
			],
			[ // the ingredient4 name 
				'label' => $model->attributeLabels()['ingredient4'],
				'value' => $model->ingredientItem4 != NULL ? $model->ingredientItem4->ingredientname :  'No ingredient was choosen',	
			],
			[ // quantity4  
				'label' => $model->attributeLabels()['quantity4'],
				'value' => $model->quantity4 != NULL ? $model->quantity4 :  'No ingredient was choosen',	
			],
            //'created_by',
			[ // User created by
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->createdBy->fullname) ? $model->createdBy->fullname : 'No one!',	
			],			
            //'created_at',
			[ // user created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],	
            //'updated_by',
			[ // User updated by
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updateddBy->fullname) ? $model->updateddBy->fullname : 'No one!',	
			],
			//'updated_at',
			[ // User updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],
        ],
    ]) ?>
</br></br>
</div>
