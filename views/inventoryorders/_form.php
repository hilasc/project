<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Ingredients;
use kartik\widgets\TouchSpin;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Inventoryorders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inventoryorders-form">
</br>
</br>
    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<?=$form->field($model, 'ingredient1')->widget(Select2::classname(), [
			'data' => $data = Ingredients::getIngridient(),
			'options' => [
			'placeholder' => 'Choose an Ingredient',
				]
			]);?>
		</div>
		<div class="col-md-2 col-md-offset-0">
				<?= $form->field($model, 'quantity1')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'Enter quantity'],
				'pluginOptions' => [
				'verticalbuttons' => true,
				'verticalupclass' => 'glyphicon glyphicon-plus',
				'verticaldownclass' => 'glyphicon glyphicon-minus',
				'min' => 0,
				'max' => 100,
				'boostat' => 5,
				]
			]);?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<?=$form->field($model, 'ingredient2')->widget(Select2::classname(), [
			'data' => $data = Ingredients::getIngridient(),
			'options' => [
			'placeholder' => 'Choose an Ingredient',
				]
			]);?>
		</div>
		<div class="col-md-2 col-md-offset-0">	
				<?= $form->field($model, 'quantity2')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'Enter quantity'],
				'pluginOptions' => [
				'verticalbuttons' => true,
				'verticalupclass' => 'glyphicon glyphicon-plus',
				'verticaldownclass' => 'glyphicon glyphicon-minus',
				'min' => 0,
				'max' => 100,
				'boostat' => 5,
				]
			]);?>
		</div>
	</div>	
	<div class="row">
		<div class="col-md-3 col-md-offset-3">	
			<?=$form->field($model, 'ingredient3')->widget(Select2::classname(), [
			'data' => $data = Ingredients::getIngridient(),
			'options' => [
			'placeholder' => 'Choose an Ingredient',
				]
			]);?>
		</div>
		<div class="col-md-2 col-md-offset-0">	
				<?= $form->field($model, 'quantity3')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'Enter quantity'],
				'pluginOptions' => [
				'verticalbuttons' => true,
				'verticalupclass' => 'glyphicon glyphicon-plus',
				'verticaldownclass' => 'glyphicon glyphicon-minus',
				'min' => 0,
				'max' => 100,
				'boostat' => 5,
				]
			]);?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">	
			<?=$form->field($model, 'ingredient4')->widget(Select2::classname(), [
			'data' => $data = Ingredients::getIngridient(),
			'options' => [
			'placeholder' => 'Choose an Ingredient',
				]
			]);?>
		</div>
		<div class="col-md-2 col-md-offset-0">		
				<?= $form->field($model, 'quantity4')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'Enter quantity'],
				'pluginOptions' => [
				'verticalbuttons' => true,
				'verticalupclass' => 'glyphicon glyphicon-plus',
				'verticaldownclass' => 'glyphicon glyphicon-minus',
				'min' => 0,
				'max' => 100,
				'boostat' => 5,
				]
			]);?>
		</div>
	</div>
	</br>
</br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
