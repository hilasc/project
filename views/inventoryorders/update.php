<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Inventoryorders */

$this->title = 'Update Inventory order #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inventory orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update Inventory order #';
?>
<div class="inventoryorders-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
