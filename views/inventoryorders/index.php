<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InventoryordersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inventory orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventoryorders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	
	<div style="text-align:right;  width: 100%;">
	<?php if (\Yii::$app->user->can('createInventoryorders')) { ?>
    <p>
        <?= Html::a('Create Inventory order', ['create'], ['class' => 'btn btn-success','style' => 'float:left;']) ?>
    </p>
		
	<!--<p>
        <?//= Html::a('export', ['export'], ['class' => 'btn btn-success']) ?>
    </p>-->
	
	<?php
                $gridColumns = [
                'id',
				'ingredient1',
                'ingredient2',
                'ingredient3',
                'ingredient4',
				'quantity1',
				'quantity2',
				'quantity3',
				'quantity4',				
            ];
		echo ExportMenu::widget([
		'dataProvider' => $dataProvider,
		'columns' => $gridColumns,
		'target' => '_self',
		'showConfirmAlert' => false,
		'filename' => 'Inventory Orders',
		'options' => ['style' => 'float:right;'],
		]);?>
		
	<?php } ?>
	</div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

            'id',
			[
				'attribute' => 'ingredient1',
				'label' => 'Ingredient 1',
				'format' => 'raw',
				'value' => function($model){
					return $model->ingredientItem1 != NULL ? $model->ingredientItem1->ingredientname :  '----';
				},
			],
			[
				'attribute' => 'quantity1',
				'label' => 'Quantity for Ing 1',
				'format' => 'raw',
				'value' => function($model){
					return $model->quantity1 != NULL ? $model->quantity1 :  '----';
				},
			],
			[
				'attribute' => 'ingredient2',
				'label' => 'Ingredient 2',
				'format' => 'raw',
				'value' => function($model){
					return $model->ingredientItem2 != NULL ? $model->ingredientItem2->ingredientname :  '----';
				},
			],
			[
				'attribute' => 'quantity2',
				'label' => 'Quantity for Ing 2',
				'format' => 'raw',
				'value' => function($model){
					return $model->quantity2 != NULL ? $model->quantity2 :  '----';
				},
			],
			[
				'attribute' => 'ingredient3',
				'label' => 'Ingredient 3',
				'format' => 'raw',
				'value' => function($model){
					return $model->ingredientItem3 != NULL ? $model->ingredientItem3->ingredientname :  '----';
				},
			],
			[
				'attribute' => 'quantity3',
				'label' => 'Quantity for Ing 3',
				'format' => 'raw',
				'value' => function($model){
					return $model->quantity3 != NULL ? $model->quantity3 :  '----';
				},
			],
			[
				'attribute' => 'ingredient4',
				'label' => 'Ingredient 4',
				'format' => 'raw',
				'value' => function($model){
					return $model->ingredientItem4 != NULL ? $model->ingredientItem4->ingredientname :  '----';
				},
			],
			[
				'attribute' => 'quantity4',
				'label' => 'Quantity for Ing 4',
				'format' => 'raw',
				'value' => function($model){
					return $model->quantity4 != NULL ? $model->quantity4 :  '----';
				},
			],
            //'quantity1',
			//'quantity2',
			//'quantity3',
			//'quantity4',
			//'created_by',
            //'created_at',
			//'updated_by',
            //'updated_at',


            ['class' => 'yii\grid\ActionColumn'],
        ],
		'responsive'=>true,
		'hover'=>true,
    ]); ?>
	</br>
</div>
