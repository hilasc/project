<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Inventoryorders */

$this->title = 'Create Inventory order';
$this->params['breadcrumbs'][] = ['label' => 'Inventory orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventoryorders-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'roles' => $roles,
    ]) ?>

</div>
