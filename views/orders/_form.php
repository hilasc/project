<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Customer;
use app\models\Productsinorder;
use app\models\Status;
use kartik\widgets\TouchSpin;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>
</br>

	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<?=$form->field($model, 'customerid')->widget(Select2::classname(), [
			'data' => $data = Customer::getCustomers(),
			]);?>
		</div>
	</div>
	<?php if ($model->isNewRecord): //CREATE?>	
	<div class="row">
		<div class="col-md-3 col-md-offset-3">	
			<?=$form->field($model, 'item1')->widget(Select2::classname(), [
			'data' => $items,
			'options' => [
			'placeholder' => 'Choose an item',
				]
			]);?>
		</div>
		<div class="col-md-2 col-md-offset-0">	
			<?=$form->field($model, 'quantity1')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'Enter quantity'],
				'pluginOptions' => [
					'verticalbuttons' => true,
					'verticalupclass' => 'glyphicon glyphicon-plus',
					'verticaldownclass' => 'glyphicon glyphicon-minus',
					'min' => 0,
					'max' => 100,
					'boostat' => 5,

				]
				]);?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<?=$form->field($model, 'item2')->widget(Select2::classname(), [
			'data' => $items,
			'options' => [
			'placeholder' => 'Choose an item',
				]
			]);?>
		</div>
		<div class="col-md-2 col-md-offset-0">	
			<?=$form->field($model, 'quantity2')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'Enter quantity'],
				'pluginOptions' => [
					'verticalbuttons' => true,
					'verticalupclass' => 'glyphicon glyphicon-plus',
					'verticaldownclass' => 'glyphicon glyphicon-minus',
					'min' => 0,
					'max' => 100,
					'boostat' => 5,
				]
				]);?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<?=$form->field($model, 'item3')->widget(Select2::classname(), [
			'data' => $items,
			'options' => [
			'placeholder' => 'Choose an item',
				]
			]);?>
		</div>
		<div class="col-md-2 col-md-offset-0">	
			<?=$form->field($model, 'quantity3')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'Enter quantity'],
				'pluginOptions' => [
					'verticalbuttons' => true,
					'verticalupclass' => 'glyphicon glyphicon-plus',
					'verticaldownclass' => 'glyphicon glyphicon-minus',
					'min' => 0,
					'max' => 100,
					'boostat' => 5,
				]
				]);?>
		</div>
	</div>
	<?php else: // UPDATE?> 
	<?php if (isset($itemsInOrder[0])): 
				$model->item1 = $itemsInOrder[0]->productnumber;
				$model->quantity1 = $itemsInOrder[0]->quantity; ?>
	<?php endif;?>
	<?php if (isset($itemsInOrder[1])): 
				$model->item2 = $itemsInOrder[1]->productnumber;
				$model->quantity2 = $itemsInOrder[1]->quantity; ?>
	<?php endif;?>
	<?php if (isset($itemsInOrder[2])): 
				$model->item3 = $itemsInOrder[2]->productnumber;
				$model->quantity3 = $itemsInOrder[2]->quantity; ?>
	<?php endif;?>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">	
			<?=$form->field($model, 'item1')->widget(Select2::classname(), [
			'data' => $items,
			'options' => [
			'placeholder' => 'Choose an item',
				]
			]);?>
		</div>
		<div class="col-md-2 col-md-offset-0">	
			<?=$form->field($model, 'quantity1')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'Enter quantity'],
				'pluginOptions' => [
					'verticalbuttons' => true,
					'verticalupclass' => 'glyphicon glyphicon-plus',
					'verticaldownclass' => 'glyphicon glyphicon-minus',
					'min' => 0,
					'max' => 100,
					'boostat' => 5,
				]
				]);?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<?=$form->field($model, 'item2')->widget(Select2::classname(), [
			'data' => $items,
			'options' => [
			'placeholder' => 'Choose an item',
				]
			]);?>
		</div>
		<div class="col-md-2 col-md-offset-0">	
			<?=$form->field($model, 'quantity2')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'Enter quantity'],
				'pluginOptions' => [
					'verticalbuttons' => true,
					'verticalupclass' => 'glyphicon glyphicon-plus',
					'verticaldownclass' => 'glyphicon glyphicon-minus',
					'min' => 0,
					'max' => 100,
					'boostat' => 5,
				]
				]);?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<?=$form->field($model, 'item3')->widget(Select2::classname(), [
			'data' => $items,
			'options' => [
			'placeholder' => 'Choose an item',
				]
			]);?>
		</div>
		<div class="col-md-2 col-md-offset-0">	
			<?=$form->field($model, 'quantity3')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'Enter quantity'],
				'pluginOptions' => [
					'verticalbuttons' => true,
					'verticalupclass' => 'glyphicon glyphicon-plus',
					'verticaldownclass' => 'glyphicon glyphicon-minus',
					'min' => 0,
					'max' => 100,
					'boostat' => 5,
				]
				]);?>
		</div>
	</div>
	<?php endif; ?>
	
	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<?= $form->field($model, 'status')->
				dropDownList(Status::getStatuses()) ?> 
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<?= $form->field($model, 'orderdate')->widget(\yii\jui\DatePicker::classname(), [
			//'language' => 'ru',
			'dateFormat' => 'dd/MM/yyyy',
			]) ?>
		</div>
	</div>
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
	</br>

</div>
