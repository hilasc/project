<?php

use yii\helpers\Html;
use app\models\Productsinorder;


/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = 'Create Order';
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'items' => $items,
    ]) ?>

</div>
