<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Productsinorder;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = 'Order #' .$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view">

    <h1><?= Html::encode($this->title) ?></h1>
<div style="text-align:right;  width: 100%;">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary','style' => 'float:left; margin-right:10px;']) ?>
		<?php if (\Yii::$app->user->can('deleteOrder')) { ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
			'style' => 'float:left;',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
		<?php } ?>
    </p>
</div>
</br></br>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
			[ // the customer name of the order
				'label' => $model->attributeLabels()['customerid'],
				'format' => 'html',
				'value' => isset($model->customerName->fullname) ? $model->customerName->fullname : 'Walk-in customer',	
			],
            'orderdate',
            'orderprice',
			[ // the status name 
				'label' => $model->attributeLabels()['status'],
				'value' => $model->statusItem->name,	
			],
            //'created_by',
			[ // User created by
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->createdBy->fullname) ? $model->createdBy->fullname : 'No one!',	
			],			
            //'created_at',
			[ // user created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],	
            //'updated_by',
			[ // User updated by
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updateddBy->fullname) ? $model->updateddBy->fullname : 'No one!',	
			],
			//'updated_at',
			[ // User updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],
			[ // Item 1
				'label' => $model->attributeLabels()['item1'],
				'value' => isset($model->getItemsInOrder($model->id)[0]) ? $model->getItemsInOrder($model->id)[0]->itemName->itemname : '------',
			],
			[ // Quantity 1
				'label' => $model->attributeLabels()['quantity1'],
				'value' => isset($model->getItemsInOrder($model->id)[0]) ? $model->getItemsInOrder($model->id)[0]->quantity : '------',
			],
			[ // Item 2
				'label' => $model->attributeLabels()['item2'],
				'value' => isset($model->getItemsInOrder($model->id)[1]) ? $model->getItemsInOrder($model->id)[1]->itemName->itemname : '------',
			],
			[ // Quantity 2
				'label' => $model->attributeLabels()['quantity2'],
				'value' => isset($model->getItemsInOrder($model->id)[1]) ? $model->getItemsInOrder($model->id)[1]->quantity : '------',
			],
			[ // Item 3
				'label' => $model->attributeLabels()['item3'],
				'value' => isset($model->getItemsInOrder($model->id)[2]) ? $model->getItemsInOrder($model->id)[2]->itemName->itemname : '------',
			],
			[ // Quantity 3
				'label' => $model->attributeLabels()['quantity3'],
				'value' => isset($model->getItemsInOrder($model->id)[2]) ? $model->getItemsInOrder($model->id)[2]->quantity : '------',
			],
        ],
    ]) ?>
</br></br>
</div>
