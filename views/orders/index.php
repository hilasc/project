<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use app\models\Orders;
use yii\data\ArrayDataProvider;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
		<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<div style="text-align:right;  width: 100%;">
		<p>
			<?= Html::a('Create Order', ['create'], ['class' => 'btn btn-success','style' => 'float:left;']) ?>
		</p>
	<!--<p>
        <?//= Html::a('export', ['export'], ['class' => 'btn btn-success']) ?>
    </p>-->
	
		
	<?php
                $gridColumns = [
                'orderId',
				'customerId',
                'orderDate',
                'orderPrice',
                'status',
				'item1',
				'qty1',
				'item2',
				'qty2',
				'item3',
				'qty3',
				
            ];
		$orders = Orders::getOrdersToExport();
		echo ExportMenu::widget([
		'dataProvider' => new ArrayDataProvider([
							'allModels' => $orders,
							]),
		'columns' => $gridColumns,
		'target' => '_self',
		'showConfirmAlert' => false,
		'filename' => 'Orders',
		'options' => ['style' => 'float:right;'],
		]);?>
		
	</div>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
			[
				'attribute' => 'customerid',
				'label' => 'Customer name',
				'format' => 'raw',
				'value' => function($model){
					return Html::a($model->customerName->fullname, 
					['customer/view', 'id' => $model->customerName->id]);
				},
				'filter'=>Html::dropDownList('OrdersSearch[customerid]', $customerid, $customerids, ['class'=>'form-control']),
			],
			[
				'attribute' => 'status',
				'label' => 'Status',
				'format' => 'raw',
				'value' => function($model){
					return $model->statusItem->name;
				},
				'filter'=>Html::dropDownList('OrdersSearch[status]', $status, $statuses, ['class'=>'form-control']),
			],
            'orderdate',
            'orderprice',
			[
				'label' => 'Item 1',
				'format' => 'raw',
				'value' => function($model){
					return (isset($model->getItemsInOrder($model->id)[0]) ? Html::a($model->getItemsInOrder($model->id)[0]->itemName->itemname,['menuitem/view', 'id' => $model->getItemsInOrder($model->id)[0]->productnumber]) : '----');
				},
				'filter'=>Html::textInput('OrdersSearch[item1]', $item1, ['class'=>'form-control']),
			],
			[
				'label' => 'Quantity 1',
				'value' => function($model){
					return (isset($model->getItemsInOrder($model->id)[0]) ? $model->getItemsInOrder($model->id)[0]->quantity : '----');
				},
				'filter'=>Html::textInput('OrdersSearch[quantity1]', $quantity1, ['class'=>'form-control']),
			],
			[
				'label' => 'Item 2',
				'format' => 'raw',
				'value' => function($model){
					return (isset($model->getItemsInOrder($model->id)[1]) ? Html::a($model->getItemsInOrder($model->id)[1]->itemName->itemname,['menuitem/view', 'id' => $model->getItemsInOrder($model->id)[1]->productnumber]) : '----');
				},
				'filter'=>Html::textInput('OrdersSearch[item2]', $item2, ['class'=>'form-control']),
			],
			[
				'label' => 'Quantity 2',
				'value' => function($model){
					return (isset($model->getItemsInOrder($model->id)[1]) ? $model->getItemsInOrder($model->id)[1]->quantity : '----');
				},
				'filter'=>Html::textInput('OrdersSearch[quantity2]', $quantity2, ['class'=>'form-control']),
			],
						[
				'label' => 'Item 3',
				'format' => 'raw',
				'value' => function($model){
					return (isset($model->getItemsInOrder($model->id)[2]) ? Html::a($model->getItemsInOrder($model->id)[2]->itemName->itemname,['menuitem/view', 'id' => $model->getItemsInOrder($model->id)[2]->productnumber]) : '----');
				},
				'filter'=>Html::textInput('OrdersSearch[item3]', $item3, ['class'=>'form-control']),
			],
			[
				'label' => 'Quantity 3',
				'value' => function($model){
					return (isset($model->getItemsInOrder($model->id)[2]) ? $model->getItemsInOrder($model->id)[2]->quantity : '----');
				},
				'filter'=>Html::textInput('OrdersSearch[quantity3]', $quantity3, ['class'=>'form-control']),
			],
            //'created_at',
            //'updated_at',
            // 'created_by',
            // 'updated_by',
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
		'responsive'=>true,
		'hover'=>true,
    ]); ?>
	</br>
</div>
