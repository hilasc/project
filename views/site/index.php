<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'HMSL Pizza System';
$this->registerJsFile('@web/js/jssor.slider-21.1.6.min.js');
?>
<div class="site-index">

    <div class="jumbotron">
        <h1 style="color:black; font-family:verdana;" ><b>Welcome to HMSL Pizza!</b></h1>
		</br></br></br>
		<!--<div>
		<img src="<?//= Yii::$app->request->baseUrl?>/Pizza.jpg" alt="Pizza HMSL" style="width:350px;height:240px;">
		</div>-->
		<div>
			<script src="<?= Yii::$app->request->baseUrl?>/js/jssor.slider-21.1.6.min.js" type="text/javascript"></script>
			<script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: true,
              $Idle: 0,
              $AutoPlaySteps: 2,
              $SlideDuration: 3400,
              $SlideEasing: $Jease$.$Linear,
              $PauseOnHover: 4,
              $SlideWidth: 280,
			  $SlideHeight: 280,
              $Cols: 5,
			  $SlideSpacing: 40,
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*responsive code begin*/
            /*you can remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 809);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*responsive code end*/
        };
    </script>
    <style></style>
    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 980px; height: 350px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('<?= Yii::$app->request->baseUrl?>/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 980px; height: 280px; overflow: hidden;">
            <div style="display: none;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/pizza1.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/pizza2.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/pizza3.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/salad1.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/salad2.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/salad3.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/bread.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/pasta1.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/pasta2.jpg" />
            </div>
        </div>
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->
		</div>
		<div style="font-size: larger; font-weight: bold;">
		<?php Yii::$app->user->isGuest ?
        print ('Please ' .Html::a('log in',['site/login']) .' to Continue') : ' '
		?>
		</div>
    </div>

</div>
