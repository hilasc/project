<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Employees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	
	<div style="text-align:right;  width: 100%;">
	<?php if (\Yii::$app->user->can('createUser')) { ?>
    <p>
        <?= Html::a('Create Employee', ['create'], ['class' => 'btn btn-success','style' => 'float:left;']) ?>
    </p>
		
	<!--<p>
        <?//= Html::a('export', ['export'], ['class' => 'btn btn-success']) ?>
    </p>-->
	
	<?php
                $gridColumns = [
                'id',
				'firstname',
                'lastname',
                'ssn',
                'username',
				'email',
				'phone',
				'startDate',
				'endDate',				
            ];
		echo ExportMenu::widget([
		'dataProvider' => $dataProvider,
		'columns' => $gridColumns,
		'target' => '_self',
		'showConfirmAlert' => false,
		'filename' => 'Employees',
		'options' => ['style' => 'float:right;'],
		]);?>
		
	<?php } ?>
	</div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            //'password',
            //'auth_key',
            'ssn',
             'firstname',
             'lastname',
             'email:email',
             'phone',
            // 'startDate',
            // 'endDate',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
		'responsive'=>true,
		'hover'=>true,
		
    ]); ?>
	</br>
</div>
