<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-form">

    <?php $form = ActiveForm::begin(); ?>
</br>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<?php if ($model->isNewRecord): ?>
			<?= $form->field($model, 'ssn')->textInput(['maxlength' => true]) ?>
			<?php endif; ?>
		</div>
		<div class="col-md-3 col-md-offset-0">
			<?php if (\Yii::$app->user->can('createUser')) { ?>
				<?= $form->field($model, 'role')->dropDownList($roles) ?>		
			<?php } ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-3 col-md-offset-0">
			<?php if ($model->isNewRecord || \Yii::$app->user->can('updatePassword') || \Yii::$app->user->can('updateOwnPassword', ['user' =>$model])): ?>
			<?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
			<?php endif; ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-3 col-md-offset-0">
			<?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-3 col-md-offset-0">
			<?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<?= $form->field($model, 'startDate')->widget(\yii\jui\DatePicker::classname(), [
			//'language' => 'ru',
			'dateFormat' => 'dd/MM/yyyy',
			])?> 
		</div>
		<div class="col-md-3 col-md-offset-0">
			<?= $form->field($model, 'endDate')->widget(\yii\jui\DatePicker::classname(), [
			//'language' => 'ru',
			'dateFormat' => 'dd/MM/yyyy',
			])?>
		</div>
	</div>
</br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</br>
    <?php ActiveForm::end(); ?>
</div>
