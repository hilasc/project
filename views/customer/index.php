<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<div style="text-align:right;  width: 100%;">
    <p>
        <?= Html::a('Create Customer', ['create'], ['class' => 'btn btn-success','style' => 'float:left;']) ?>
    </p>
	
	<!--<p>
        <?//= Html::a('export', ['export'], ['class' => 'btn btn-success']) ?>
    </p>-->
	<?php
                $gridColumns = [
                'id',
				'firstname',
                'lastname',
				'phone',
				'address',			
            ];
		echo ExportMenu::widget([
		'dataProvider' => $dataProvider,
		'columns' => $gridColumns,
		'target' => '_self',
		'showConfirmAlert' => false,
		'filename' => 'Customers',
		'options' => ['style' => 'float:right;'],
		]);?>
		
	</div>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn'],

            'id',
            'firstname',
            'lastname',
            'phone',
            'address',

            ['class' => 'yii\grid\ActionColumn'],
        ],
		'responsive'=>true,
		'hover'=>true,
    ]); ?>
	</br>
</div>
