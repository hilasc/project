<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">
</br>
    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	</br>
	</br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
