<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Menuitem */

$this->title = 'Update Menu item: ' . $model->itemname;
$this->params['breadcrumbs'][] = ['label' => 'Menu items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->itemname, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="menuitem-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
