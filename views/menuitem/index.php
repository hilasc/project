<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MenuitemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menu items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menuitem-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	
	<div style="text-align:right;  width: 100%;">
	<?php if (\Yii::$app->user->can('createMenuitem')) { ?>
    <p>
        <?= Html::a('Create Menu item', ['create'], ['class' => 'btn btn-success','style' => 'float:left;']) ?>
    </p>
	
	<?php
                $gridColumns = [
                'id',
				'itemname',
                'itemprice',			
            ];
		echo ExportMenu::widget([
		'dataProvider' => $dataProvider,
		'columns' => $gridColumns,
		'target' => '_self',
		'showConfirmAlert' => false,
		'filename' => 'Menu Items',
		'options' => ['style' => 'float:right;'],
		]);?>
		
	<?php } ?>
	</div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'itemname',
			'itemprice',
            //'created_at',
            //'updated_at',
            //'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
		'responsive'=>true,
		'hover'=>true,
    ]); ?>
	</br>
</div>
