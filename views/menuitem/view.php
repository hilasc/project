<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Menuitem */

$this->title = $model->itemname;
$this->params['breadcrumbs'][] = ['label' => 'Menu items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menuitem-view">

    <h1><?= Html::encode($this->title) ?></h1>
	<div style="text-align:right;  width: 100%;">
	<?php if (\Yii::$app->user->can('deleteMenuitem')) { ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary','style' => 'float:left; margin-right:10px;']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
			'style' => 'float:left;',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
	<?php } ?>
</div>
</br></br>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'itemname',
			'itemprice',
            //'created_by',
			[ // User created by
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->createdBy->fullname) ? $model->createdBy->fullname : 'No one!',	
			],			
            //'created_at',
			[ // user created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],	
            //'updated_by',
			[ // User updated by
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updateddBy->fullname) ? $model->updateddBy->fullname : 'No one!',	
			],
			//'updated_at',
			[ // User updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],
        ],
    ]) ?>
</br></br>
</div>
