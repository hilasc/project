<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\TouchSpin;

/* @var $this yii\web\View */
/* @var $model app\models\Menuitem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menuitem-form">
</br>
</br>
    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<?= $form->field($model, 'itemname')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<?= $form->field($model, 'itemprice')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'Enter item price'],
				'pluginOptions' => [
				'verticalbuttons' => true,
				'verticalupclass' => 'glyphicon glyphicon-plus',
				'verticaldownclass' => 'glyphicon glyphicon-minus',
				'min' => 0,
				'max' => 10000,
				'boostat' => 5,
				'prefix' => '$',
				]
			]);?>
		</div>
	</div>
</br>
</br>
</br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
