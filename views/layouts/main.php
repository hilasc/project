<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'HMSL PIZZA',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
			Yii::$app->user->isGuest ?
            ['label' => 'Home', 'url' => ['/site/index']]
			:
			['label' => 'Home', 'url' => ['/site/index']],
			Yii::$app->user->isGuest ?
			' '
			:
            ['label' => 'Employees', 'url' => ['/employee/index']],
			Yii::$app->user->isGuest ?
			' '
			:
			['label' => 'Customers', 'url' => ['/customer/index']],
			Yii::$app->user->isGuest ?
			' '
			:
			['label' => 'Orders', 'url' => ['/orders/index']],
			Yii::$app->user->isGuest ?
			' '
			:
			['label' => 'Menu items', 'url' => ['/menuitem/index']],
			Yii::$app->user->isGuest ?
			' '
			:
			['label' => 'Inventory orders', 'url' => ['/inventoryorders/index']],
			Yii::$app->user->isGuest ?
			['label' => 'About', 'url' => ['/site/about']]
			:
            ['label' => 'Ingredients', 'url' => ['/ingredients/index']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>	
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; HMSL PIZZA <?= date('Y') ?></p>
		<p style="float:right;"> Hila Zarifi 203919642, &nbsp Menachem Twersky 201649514, &nbsp Shai Saposh 302471107, &nbsp Laury Aziza 332702257 &nbsp &nbsp</p>
      
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
